# IIoT Protocols

![IIoT protocols](https://img.deusm.com/eetimes/2015/03/1326169/bccPrismTechIIotAltsTable.jpg)

**Note**-Few Important Protocols mentioned below according to our Internship Wiki.


## Modbus Communication Protocol

- Modbus functions perform read and write instructions to the slave's internal memory registers to configure, monitor, and control the slave's inputs and outputs.It is typically used to transmit signals from instrumentation and control devices back to a main controller, or data gathering system.
- The devices include a register map outlining where the configuration, input and output data can be written and read from.
- It is a method used for transmitting information over serial lines between electronic devices. The device requesting the information is called the **Modbus Master** and the devices supplying information are **Modbus Slaves**.
- In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.
- Modbus is a communication protocol for use with programmable logic controllers (PLC). 

### How MODBUS protocol works?
- Communication between a master and a slave occurs in a frame that indicates a function code.
- The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.
- The slave then responds, based on the function code received.
- The protocol is commonly used in IoT as a local interface to manage devices.

**Modbus protocol can be used over 2 interfaces**
- RS485 - called as Modbus RTU
- Ethernet - called as Modbus TCP/IP


![MODBUS](https://realpars.com/wp-content/uploads/2018/12/How-does-Modbus-Communication-Protocol-Work-Between-Devices-1.png)


### RS485

- RS485 is a serial data transmission standard widely used in industrial implementations. 
- RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
- RS485 is not directly compatible: you must use the correct type of interface, or the signals won't go through. Mainly done through an easy to use an RS485 to USB.

**The Modbus protocol is commonly used when implementing RS485 communication. This differentiates RS485 from the RS232 protocol which communicates by transmitting with ASCII characters.**

### How RS485 MODBUS Protocol works?
The MODBUS RS485 protocol defines communication between a host (master) and devices (slaves) that allows querying of device configuration and monitoring. MODBUS messages relay simple read and write operations on 16 bit words and binary registers often referred to as “coils”



## OPC Unified Architecture(OPCUA)
- Machine to machine communication protocol for industrial automation developed by the OPC Foundation.
- Focus on communicating with industrial equipment and systems for data collection and control.
- Implements a sophisticated Security Model that ensures the authentication of Client and Servers, the authentication of users and the integrity of their communication.OPC UA is designed to connect Objects in such a way that true Information can be shared between Clients and Servers.


| Port | Transport Layer Description |
| ------ | ------ |
| 4840 | tcp	OPC UA Connection Protocol |
| 4840 | udp	OPC UA Multicast Datagram Protocol |


![opcua](https://www.novotek.com/images/solutionpages/Kepware_solutionpages/2015_OPC_client_server.png)

![OPCUA](https://readthedocs.web.cern.ch/download/attachments/21178238/general%20diagram.png?version=1&modificationDate=1427214493000&api=v2)


## 4-20 mA

- The 4-20 mA current loop is the prevailing process control signal in many industries.
- Ideal method of transferring process information because current does not change as it travels from transmitter to receiver. Simpler and cost effective.(voltage drops and the number of process variables that need to be monitored can impact its cost and complexity.)


![Components](https://www.predig.com/sites/default/files/images/Indicator/back_to_basics/4-20mA_Current_Loops/4-20mA_current_loop_components.jpg)

### Pros & Cons of 4-20 mA Loops
Part of the challenge of working in an industry which requires process control is determining if the pros outweigh the cons. Making the right decision can save both time and money.

**Pros**
- The 4-20 mA current loop is the dominant standard in many industries.
- It is the simplest option to connect and configure.
- It uses less wiring and connections than other signals, greatly reducing initial setup costs.
- Better for traveling long distances, as current does not degrade over long connections like voltage.
- It is less sensitive to background electrical noise.
- Since 4 mA is equal to 0% output, it is incredibly simple to detect a fault in the system.

**Cons**
- Current loops can only transmit one particular process signal.
- Multiple loops must be created in situations where there are numerous process variables that require transmission. Running so much wire could lead to problems with ground loops if independent loops are not properly isolated.
- These isolation requirements become exponentially more complicated as the number of loops increases.

# MQTT (Message Queuing Telemetry Transport) :

- A simple messaging protocol designed for constrained devices with low bandwidth.
- A lightweight publish and subscribe system to publish and receive messages as a client.
- A perfect solution for IOT applications, also allows to send commands to control outputs, read and publish data from sensor nodes.
- Communication between several devices can be established simultaneously. 

## MQTT works?
MQTT Client as a publisher sends a message to the MQTT broker whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.
Topics are a way to register interest for incoming messages or to specify where to publish the message. Represented by strings, separated by forward slash. Each slash indicates a topic level.

![MQTT](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/358848b4b9222917e31db143ff0a6383/Main-entities-of-the-Message-Queuing-Telemetry-Transport-MQTT-protocol.png)


# HTTP 

- It is a request response protocol   
- The client sends an HTTP request 
- The server sends back a HTTP response

## The Request
request has 3 parts
- Request line
- HTTP headrs
- message body
  
**GET request**-it is a type of HTTP request using the GET method
there are different types of methods- 
- POST-Create a resource on the server (e.g. when submitting a form);  
- PUT/PATCH-Update the resource on the server (used by APIs);  
- DELETE-Delete the resource from the server (used by APIs). 
- GET-Retrieve the resource from the server (e.g. when visiting a page);


## The response
response is made of 3 parts
- Status line 
- HTTP header
- Message body


![https](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/85f443d40e36e1f5bcb4e71ac14205b0/http-req-res.png)

</html>
```

The server will send you send you different status codes depending on situation   
