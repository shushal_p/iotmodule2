# Board Electrical Safety Instructions

## What is electrical safety?

Electrical safety is a system of organizational measures and technical means to prevent harmful and dangerous effects on workers from electric current, electric arc, electromagnetic field and static electricity.

## Dos and Don'ts

### Power Supply

#### Dos:

 - Always make sure that the output voltage of the power supply matches the input voltage of the board.
 - While turning on the power supply make sure every circuitbOARDis connected properly.
 - The Output voltage of the power supply should match the input voltage of the board.
 - Before turning on the power supply make sure circuit is connected properly.

#### Don’ts:

 - Do not connect power supply without matching the power rating.
 - Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input.
 - Do not try to force the connector into the power socket ,it may damage the connector.

![PS](https://image.slidesharecdn.com/hazardsandrisks-161118031400/95/hazards-and-risks-12-638.jpg?cb=1479438913)


### Handling

#### Dos:

 - Treat every device like it is energized, even if it does not look like it is plugged in or operational.
 - While working keep the board on a flat stable surface (wooden table).
 - Unplug the device before performing any operation on them.
 - When handling electrical equipment, make sure your hands are dry.
 - Keep all electrical circuit contact points enclosed.
 - If the board becomes too hot try to cool it with a external usb fan.

#### Don’ts:

 - Do not handle the board when it is powered ON.
 - Never touch electrical equipment when any part of your body is wet (that includes fair amounts of perspiration).
 - Do not touch any sort of metal to the development board.
![Handling](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRv7jevnEnn-kD65keLC-yxW1669juWByB0eQ&usqp=CAU)

## IoT and Safety Hazards

There is a high-level summary of standards on the horizon for the connected world.
Using IoT sensors is a cheap and effective way to ensure that all electric circuits are functioning correctly. These can collect data about tension, voltage, the Joule effect and prevent overcharging, power outages or even fires caused by short-circuits. Computer vision can process a wide range of different signals coming from electric circuits and detect abnormal activity.
The Internet of things (IoT) is taking the concept of safety to a whole new level. In a world where everything can be connected, and physical contact becomes optional, verbal instructions and even facial recognition are used to operate the end device, safety considerations must be made.
- **Type 1 hazards** are directly associated with the traditional use of the device and include things such as: 
overheating, shock, sonic hazards, etc. Introducing IoT into products could increase the frequency of occurrence of these hazards. Enabling remote operations means that hazards of these kinds are no longer bounded by physical limitations.
- **Type 2 hazards** are indirectly related to the device and its operation but could enable a security or safety issue by implementing IoT. The cascade effect could be a breaching of properties or leakage of private information. 


## GPIO

- GPIO stands for General Purpose Input/Output. It's a standard interface used to connect microcontrollers to other electronic devices. For example, it can be used with sensors, diodes, displays, and System-on-Chip modules.
- Connecting a GPIO or other pin to the wrong voltage can easily damage the board and render it unusable. Static electricity, especially during the dry winter months, can also generate high voltages that can damage hardware if you develop a static charge and then touch the board.

**Do’s:**

- Find out whether the board runs on 3.3v or 5v logic.
- Always connect the LED (or sensors) using appropriate resistors .
- To Use 5V peripherals with 3.3V we require a logic level converter.

**Don’ts:**

- Never connect anything greater that 5v to a 3.3v pin.
- Avoid making connections when the board is running.
- Don't plug anything with a high or negative voltage.
- Do not connect a motor directly , use a transistor to drive it .


## Guidelines for using UART interface

A universal asynchronous receiver-transmitter is a computer hardware device for asynchronous serial communication in which the data format and transmission speeds are configurable. The electric signaling levels and methods are handled by a driver circuit external to the UART.

- Connect Rx pin of device1 to Tx pin of device2 ,similarly Tx pin of device1 to Rx pin of device2.
- If the device1 works on 5v and device2 works at 3.3v,use the level shifting mechanism using **Volatge divider**
- General USB to TTL connection to communicate through board does not require a protection circuit .
- Senor interfacing using UART might require a protection circuit.

![UART](https://www.elprocus.com/wp-content/uploads/UART.jpg) ![UART1](https://www.codrey.com/wp-content/uploads/2017/10/UART-Block-Diagram.png)


## Guidelines for using I2C interface

I2C is a serial protocol for two-wire interface to connect low-speed devices like microcontrollers, EEPROMs, A/D and D/A converters, I/O interfaces and other similar peripherals in embedded systems.

- When using I2c interfaces with sensors SDA and SDL lines must be protected.
- Protection of these lines is done by using pullup registers on both lines.
- while using inbuilt pullup registers in the board you wont need an external circuit.
- if using bread-board to connect sensor , use the pullup resistor .
- Generally , 2.2kohm <= 4K ohm resistors are used.

![I2C](https://electrosome.com/wp-content/uploads/2018/02/I2C-Interface.png
)

## Guidelines for using SPI interface

The Serial Peripheral Interface is a synchronous serial communication interface specification used for short-distance communication, primarily in embedded systems.

- Spi in development boards is in Push-pull mode.
- Push-pull mode does not require any protection circuit.
- If using more than one slaves it is possible that the device2 can hear and respond to the master's communication with device1 which is disturbance .
- To avoid such interference we use a protection circuit with pullup resistors on each the Chip Select/Slave Select line.
- Resistors value should be in between 1kOhm ~10kOhm,Preferably 4.7k Ohm resistor.

![SPI](https://electrosome.com/wp-content/uploads/2017/04/SPI-Communication.png)

